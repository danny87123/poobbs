import java.io.*;
import java.util.Scanner;
import java.util.LinkedList;


public class POOArticle{
       int ID;
	   String title;
	   String author;
	   String content;
	   int evaluationCount = 0;
	   
	   /* 
	      flag = 1 -> push
	      flag = -1 -> boo
		  flag = 0 -> arrow
	   */
	   public class messageLine{
	       String message;
		   int flag;
	   }
	   
	   static int MAXEVAL;

	   LinkedList <messageLine> evaluationMessages = new LinkedList <messageLine>();
	   
	   public POOArticle(int articleID,String articleAuthor,String articleTitle,String articleContnet){
	        ID =  articleID;
            title = articleTitle;		
	        author = articleAuthor;
			content = articleContnet;
	   }
	   
	   public void push(String response){
	        evaluationMessages.add (new messageLine());
			evaluationMessages.get(evaluationMessages.size()-1).message = new String(response);
	        evaluationMessages.get(evaluationMessages.size()-1).flag = 1;
	        evaluationCount++;
	   }
	   
	   public void boo(String response){
	        evaluationMessages.add (new messageLine());
			evaluationMessages.get(evaluationMessages.size()-1).message = new String(response);
	        evaluationMessages.get(evaluationMessages.size()-1).flag = -1;
	        evaluationCount--;
	   }
	   
	   public void arrow(String response){
	        evaluationMessages.add (new messageLine());
			evaluationMessages.get(evaluationMessages.size()-1).message = new String(response);
	        evaluationMessages.get(evaluationMessages.size()-1).flag = 0;
	   }
	   
	   public void show(){
	        int i;
			System.out.println("ID     "+ID);
			System.out.println("author "+author);
			System.out.println("title  "+title);
			System.out.println("");
			System.out.println("");
			System.out.println(content);
			System.out.println("");
			System.out.println("");
			for(i=0;i<evaluationMessages.size();i++){
			   switch(evaluationMessages.get(i).flag){
			         case 1:
					     System.out.print("push ");
						 break;
					 case -1:
					     System.out.print("boo  ");
						 break;
					 case 0:
					     System.out.print("->   ");
						 break;
					 default: ;
                   }
                System.out.println(evaluationMessages.get(i).message);  
			 }
      }

      public void list(int i){
           System.out.printf("%4d   %3d   %3d   %s    %s",i+1,ID,evaluationCount,author,title);
            System.out.println("");
      }
}	  