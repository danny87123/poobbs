import java.io.*;
import java.util.Scanner;
import java.util.LinkedList;


public class POODirectory{
       public String directoryName;
	   
	   public LinkedList <Directory> mainDirectory = new LinkedList <Directory>();
	   
	   //create a directory with the name
	   public POODirectory(String name){
	          directoryName = name;
	   }
	   
	   public class Directory{
	          int split;
			  POODirectory directory;
			  POOBoard board;
	   }
	   
	   //append the board to the directory
       public void add(POOBoard board){
              Directory tmp = new Directory();
			  tmp.split = 0;
			  tmp.board = board;
			  mainDirectory.add(tmp);
       }

	   //append the directory to the directory
	   public void addDirectory(POODirectory dir){
              Directory tmp = new Directory();
			  tmp.split = -1;
			  tmp.directory = dir;
			  mainDirectory.add(tmp);
       }

	   //append a splitting line to the directory
	   public void addSplit(){
              Directory tmp = new Directory();
			  tmp.split = 1;
	          mainDirectory.add(tmp);
	   }

	   //delete the board/directory/splitting line at position pos
	   public void del(int pos){
              mainDirectory.remove(pos);
       }
	   
	   //move the board/directory/splitting line at position src to position dest
	   public void move(int src, int dest){
              Directory tmp;
			  tmp = mainDirectory.get(src);
	          mainDirectory.add(dest,tmp); 
	          mainDirectory.remove(src);
	   }
       
	   //get the current number of items in the directory
	   public int length(){
              return mainDirectory.size();  
       }

	   //show the board/directory titles and splitting lines of the directory
	   public void show(){
              int i;
			  for(i=0;i<mainDirectory.size();i++){
                 System.out.print((i+1)+"  ");
				 switch(mainDirectory.get(i).split){
                       case 1:
                           System.out.println("------------");
                           break;
                       case 0:
                           System.out.println(mainDirectory.get(i).board.boardName+"        board");
                           break;
					   case -1:
                    	   System.out.println(mainDirectory.get(i).directory.directoryName+"        directory");
                       default: ;
                   }						   
                }
	  }


}