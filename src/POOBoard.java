import java.io.*;
import java.util.LinkedList;


public class POOBoard{
       public String boardName;
	   public int allNumber=0;
	   
	   LinkedList <POOArticle> board = new LinkedList <POOArticle>();
	   
	   
	   //create a board with the name
	   public POOBoard(String name){
           boardName = new String(name);     
	   }
       
	   //append the article to the board
	   public void add(POOArticle article){
		   board.add (article); 
       }
       
	   //delete the article at position pos
	   public void del(int pos){
           board.remove(pos);
       }
       
	   //move the article at position src to position dest
	   public void move(int src, int dest){
           board.add(dest,board.get(src)); 
           board.remove(src);
	   }
       
	   //get the current number of articles in the board
	   public int length(){
          return board.size();
       }

	    //show the article titles of the board
	   public void show(){
          int i;
		  System.out.printf(" NO.   title\n");
		  for(i=0;i<board.size();i++){
		     System.out.printf("%4d   ",i+1);
			 System.out.println(board.get(i).title);
          }
	   }
	   
	   //show the article information of the board
	   public void showInformation(){
          int i;
		  System.out.printf(" NO.    ID   hot     author       title\n");
		  for(i=0;i<board.size();i++)
			 board.get(i).list(i);
       }

}