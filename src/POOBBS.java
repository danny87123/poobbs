import java.io.*;
import java.util.*;

public class POOBBS{
   
        static LinkedList <POOBoard> bbs = new LinkedList <POOBoard>();
			  
		static String username;
		
			  
		static POODirectory myFavorite = new POODirectory("myFavorite");
		   
   
		public static void main(String args[]){
              Scanner scanner = new Scanner (System.in);
			  int i,index;
			  int action;
			  POOBoard board;
			  System.out.println(" ");
			  System.out.println(" ");
			  System.out.println("welcome to BBS");
			  System.out.println(" ");
			  System.out.println(" ");
			  System.out.print("Please enter your name: ");
			  username = scanner.next();
			  System.out.println(" ");
			  System.out.println(" ");
		      System.out.println("Welcome, " + username);
			  while(true){
			   System.out.println(" ");
			   System.out.println(" ");
			   System.out.println("BBS main page");
			   System.out.println(" ");
			   System.out.println(" ");
			   System.out.println("what do you want to do?");
			   System.out.print("(1) create board  (2) go to board (3) my favorite (4) exit : ");
			   action = scanner.nextInt();
			   if(action==1)
			     createBoard();
               else if(action==2){
			     System.out.println(" ");
				 System.out.printf(" NO.   Board\n");
				 for(i=0;i<bbs.size();i++){
                   System.out.printf("%4d   ",i+1);
				   System.out.println(bbs.get(i).boardName);
                 }
				 System.out.println(" ");
				 System.out.print("which board do you want to go?   (Please enter the No.) : ");
                 index = scanner.nextInt();
                 if(index > bbs.size())
				    System.out.print("can't find this board");
				 board = bbs.get(index-1);
                 goBoard(board);
				}
			   else if(action==3)
			     goToDirectory(myFavorite);
			   else if(action==4){
			     System.out.println("bye bye");
			     return;
			   }
	          }	
		
		}				
			  
		public static void goToDirectory(POODirectory dir){
               int i,num,flag=0,index;
			   Scanner scanner = new Scanner (System.in);
			   String name;
			   while(true){
			   flag = 0;
			   System.out.println(" ");
			   System.out.println(dir.directoryName);
			   System.out.println(" ");
			   dir.show();
			   System.out.println(" ");
			   System.out.println("what do you want to do?");
			   System.out.print("(1) add directory (2) add board (3) add split (4) go to board (5) go to directory (6) delete (7) move (8) exit : ");
			   num = scanner.nextInt();
			   if(num==1){
			      System.out.println("please enter the directory name:");
			      name = scanner.next();
				  POODirectory newDirectory = new POODirectory(name);
				  dir.addDirectory(newDirectory);
			   }
			   else if(num==2){
			      System.out.println(" ");
			      System.out.println(" ");
				  System.out.println("please enter the board name:");
			      name = scanner.next();
				  POOBoard board;
				  for(i=0;i<bbs.size();i++){
				     if(name.equals(bbs.get(i).boardName)==true){
					    dir.add(bbs.get(i));
						flag = 1;
				        break;
					  }
				  }
				  if(flag==0)
				    System.out.println("the board does not exist");
               }
              else if(num==3)
			      dir.addSplit();
			  else if(num==4){
                  System.out.println(" ");
			      System.out.println(" ");
				  dir.show();
				  System.out.printf("which board do you want to go? (please enter the number) :  ");
                  index = scanner.nextInt();			  
				  POOBoard Board;
				  if(dir.mainDirectory.get(index-1).split==0){
				    Board = dir.mainDirectory.get(index-1).board;
					goBoard(Board);
				  }
				 else {
				     System.out.println("this is not board");
				  }
			  }
			   else if(num==5){
			      System.out.println(" ");
			      System.out.println(" ");
				  dir.show();
				  System.out.printf("which directory do you want to go? (Please enter the number) :  ");
				  index = scanner.nextInt();
				  POODirectory childDirectory;
				  if(dir.mainDirectory.get(index-1).split==-1){
				     childDirectory = dir.mainDirectory.get(index-1).directory;
				     goToDirectory(childDirectory);
				  }
				  else {
				     System.out.println("this is not directory");
				   }
				}
				else if(num==6){
				  System.out.println(" ");
			      System.out.println(" ");
				  dir.show();
				  System.out.print("which one do you want to delete? : ");
				  index = scanner.nextInt();
				  dir.del(index-1);
				}
				else if(num==7){
				  directoryMove(dir);  
				}
				else
				    return;
		     }
		  }
        
        public static void directoryMove(POODirectory dir){
		       Scanner scanner = new Scanner (System.in);
			   int src,dest;
			   dir.show();
			   System.out.println("which one do you want to move? : ");
			   src = scanner.nextInt();
			   System.out.println("please enter the destination : ");
               dest = scanner.nextInt();
			   dir.move(src-1,dest);
	           System.out.println("remove successful!!!");
		       //dir.show();
		}		
	    
		public static void createBoard(){
		      Scanner scanner = new Scanner (System.in);
			  String name;
			  System.out.println(" ");
			  System.out.println(" ");
			  System.out.println("please enter the board name");
			  name = scanner.next();
			  POOBoard board = new POOBoard(name);
			  bbs.add(board);
			  System.out.println("create successful");
	    }
		
		public static void goBoard(POOBoard board){
		      int i;
			  int flag,index;
              Scanner scanner = new Scanner (System.in);
			  while(true){
			         System.out.println(" ");
					 System.out.println(" ");
					 System.out.println("Now in "+board.boardName);
					 System.out.println(" ");
					 System.out.println(" ");
					 System.out.println("what do you want to do?");
					 System.out.print("(1) show article title  (2) show article information (3) create article (4) read article (5) delete article (6) move article (7) exit : ");
					 flag = scanner.nextInt();
					 if(flag==1)
					   board.show();
				     else if(flag==2)
                       board.showInformation();
					 else if(flag==3){
                       try{
					       createArticle(board);
					   }
					   catch(IOException e){
					       System.out.println("IOException");
						  }
				     }
                     else if(flag==4){
                       try{
					       readArticle(board);
					   }
					   catch(IOException e){
					       System.out.println("IOException");
						  }
					 }
                     else if(flag==5){
					   System.out.println("which article do you want to delete?");
					   board.show();
					   index = scanner.nextInt();
					   if(username.equals(board.board.get(index-1).author)!=true){
					     System.out.println("the permission error!!!!");
					   }
					 else{
					     board.del(index-1);
					     System.out.println("delete successful!!!");
					    }
					 }
					 else if(flag==6){
					     moveArticle(board);
					 }
					 else
                       return;
              }
        }
        
		public static void moveArticle(POOBoard board){
		       Scanner scanner = new Scanner (System.in);
			   int src,dest;
			   board.show();
			   System.out.println("which article do you want to move? : ");
			   src = scanner.nextInt();
			   System.out.println("please enter the destination : ");
               dest = scanner.nextInt();
			   board.move(src-1,dest-1);
	           System.out.println("remove successful!!!");
		       board.show();
		}
		
		public static void createArticle(POOBoard board) throws IOException {
               Scanner scanner = new Scanner (System.in);
			   BufferedReader buf = new BufferedReader(
                                 new InputStreamReader(System.in)); 
			   board.allNumber++;
			   int ID = board.allNumber;
			   String author = new String(username);
			   String title;
			   String content;
			   System.out.println(" ");
			   System.out.println(" ");
			   System.out.println("Now create article");
			   System.out.print("Please enter the title: ");
			   title = buf.readLine();
			   System.out.print("Please enter the content: ");
			   content = buf.readLine();
			   POOArticle article = new POOArticle(ID,author,title,content);
			   board.add(article);
		}
		
		public static void readArticle(POOBoard Board) throws IOException {
		       Scanner scanner = new Scanner (System.in);
			   BufferedReader buf = new BufferedReader(
                                 new InputStreamReader(System.in)); 
			   int i,num,choose,flag=0;
			   POOArticle article;
			   String message;
			   Board.showInformation();
			   System.out.print("which article do you want to read? : (Please enter the board ID) ");
			   num = scanner.nextInt();
		       if(Board.board.size()==0){
			     System.out.println("no artical");
			     return;
				}
			   article = Board.board.get(0);
			   for(i=0;i<Board.board.size();i++){
			      if(Board.board.get(i).ID == num){
				     flag = 1;
					 article = Board.board.get(i);
			         article.show();
			       }
			   }
			   if(flag==0){
			      System.out.println("no this article");
			      return;
				}
			   while(true){ 
				System.out.print("what do you want to do ? : ");
			    System.out.print("(1) push (2) boo (3) arrow (4) exit");
			    choose = scanner.nextInt();
			    if(choose==4)
			      return;
				if(choose==1){
				  System.out.println("please enter your response : ");
				  message = buf.readLine();
				  article.push(message);
				}
				else if(choose==2){
				  System.out.println("please enter your response : ");
				  message = buf.readLine();
				  article.boo(message);
				}
                else if(choose==3){
				  System.out.println("please enter your response : ");
				  message = buf.readLine();
				  article.arrow(message);
				} 				
			   }
		 }

}		 